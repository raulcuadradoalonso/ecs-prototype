# ECS Prototype

Simple ECS structure created for learning purposes. Tested adding a pair of custom components and showing its simple behavior through console.

## Demo

I coded a pretty simple demo just to make sure that all the systems and templates were working correctly. The console output is:

```bash
--------------------------------------------------------
************     ECS architecture demo!     ************
--------------------------------------------------------

-------------------------------
*******  Initial setup  *******
-------------------------------

Systems:
* Adding Cool system.
* Adding Renderer system.

Entities:
* Creating entity #1.
*   - Added Cool component.
*   - Added Transform component.
*   - Added Renderable component.
* Creating entity #2.
*   - Added Cool component.
*   - Added Renderable component.

-------------------------------
*******    Game loop    *******
-------------------------------

 - Updating cool component.
 - Updating cool component.
 - Rendering entity.

--------------------------------------------------------
************        End of the demo.        ************
--------------------------------------------------------
```

As can be seen, I make 2 entities. One system just needs the Cool component, but the other one needs both transform and renderable components. Since the second entity has only one of them, we can see in the loop example that only the first entity is being rendered.