#include "pch.h"
#include "ECS.h"

struct CoolComponent : public ECSComponent<CoolComponent>
{
	std::string message = "I'm cool component!";
};

struct TransformComponent : public ECSComponent<TransformComponent>
{
	std::string message = "I'm transform component!";
};

struct RenderableComponent : public ECSComponent<RenderableComponent>
{
	std::string message = "I'm renderable component!";
};

class CoolSystem : public BaseECSSystem
{
public:
	CoolSystem() : BaseECSSystem()
	{
		addComponentType(CoolComponent::ID);
	}

	virtual void updateComponents(float delta, BaseECSComponent** components) override
	{
		CoolComponent* fc = (CoolComponent*)components[0];
		std::cout << " - Updating cool component." << std::endl;
	}
};

class RendererSystem : public BaseECSSystem
{
public:
	RendererSystem() : BaseECSSystem()
	{
		addComponentType(TransformComponent::ID);
		addComponentType(RenderableComponent::ID);
	}

	virtual void updateComponents(float delta, BaseECSComponent** components) override
	{
		std::cout << " - Rendering entity." << std::endl;
	}
};


int main()
{
	std::cout << "--------------------------------------------------------" << std::endl;
	std::cout << "************     ECS architecture demo!     ************" << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl;
	std::cout << std::endl;

	ECS ecs;
	ECSSystemList systems;

	std::cout << "-------------------------------" << std::endl;
	std::cout << "*******  Initial setup  *******" << std::endl;
	std::cout << "-------------------------------" << std::endl;
	std::cout << std::endl;

	//-----Systems
	std::cout << "Systems: " << std::endl;

	//System #1
	std::cout << "* Adding Cool system." << std::endl;
	CoolSystem fcs;
	systems.addSystem(fcs);
	//System #2
	std::cout << "* Adding Renderer system." << std::endl;
	RendererSystem rs;
	systems.addSystem(rs);

	std::cout << std::endl;

	//-----Entities
	std::cout << "Entities: " << std::endl;
	//Entity #1
	std::cout << "* Creating entity #1." << std::endl;
	CoolComponent coolComponent1;
	std::cout << "*   - Added Cool component." << std::endl;
	TransformComponent transformComponent1;
	std::cout << "*   - Added Transform component." << std::endl;
	RenderableComponent renderableComponent1;
	std::cout << "*   - Added Renderable component." << std::endl;
	ecs.makeEntity(coolComponent1, transformComponent1, renderableComponent1);
	//Entity #2
	std::cout << "* Creating entity #2." << std::endl;
	CoolComponent coolComponent2;
	std::cout << "*   - Added Cool component." << std::endl;
	RenderableComponent renderableComponent2;
	std::cout << "*   - Added Renderable component." << std::endl;
	ecs.makeEntity(coolComponent2, renderableComponent2);


	std::cout << std::endl;
	std::cout << "-------------------------------" << std::endl;
	std::cout << "*******    Game loop    *******" << std::endl;
	std::cout << "-------------------------------" << std::endl;
	std::cout << std::endl;

	//Let's pretend this is a game loop with just one iteration
	//Arbitrary delta value
	ecs.updateSystems(systems, 0.333f);

	std::cout << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl;
	std::cout << "************        End of the demo.        ************" << std::endl;
	std::cout << "--------------------------------------------------------" << std::endl;
}