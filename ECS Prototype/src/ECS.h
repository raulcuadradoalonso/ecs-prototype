#pragma once
#include "ECSComponent.h"
#include "ECSSystem.h"
#include <map>

class ECS
{
public:

	ECS() {}
	~ECS();

	ECS(ECS &ecs) = delete;
	ECS operator=(const ECS& ecs) = delete;

	//-----Entity methods
	EntityHandle makeEntity(BaseECSComponent** components, const uint32_t* componentIDs, size_t numComponents);
	void removeEntity(EntityHandle handle);

	template<class ...ARGS> EntityHandle makeEntity(ARGS&... _args)
	{
		BaseECSComponent* components[] = { &_args... };
		unsigned int componentIDs[] = { ARGS::ID... };
		return makeEntity(components, componentIDs, sizeof...(ARGS));
	}

	//-----Component methods
	template<class Component>
	inline void addComponent(EntityHandle entity, Component* component)
	{
		addComponentInternal(entity, handleToEntity(entity), Component::ID, component);
	}

	template<class Component>
	bool removeComponent(EntityHandle entity)
	{
		return removeComponentInternal(entity, Component::ID);
	}

	template<class Component>
	Component* getComponent(EntityHandle entity)
	{
		return (Component*)getComponentInternal(handleToEntity(entity), m_components[Component::ID], Component::ID);
	}

	//-----System methods
	void updateSystems(ECSSystemList& systems, float delta);

private:

	std::vector<BaseECSSystem*> m_systems;
	std::map<uint32_t, std::vector<uint8_t>> m_components;
	std::vector < std::pair<uint32_t, std::vector<std::pair<uint32_t, uint32_t>>>* > m_entities;

	inline std::pair<uint32_t, std::vector<std::pair<uint32_t, uint32_t>>>* handleToRawType(EntityHandle handle)
	{
		return (std::pair<uint32_t, std::vector<std::pair<uint32_t, uint32_t>>>*) handle;
	}

	inline uint32_t handleToEntityIndex(EntityHandle handle)
	{
		return handleToRawType(handle)->first;
	}

	inline  std::vector<std::pair<uint32_t, uint32_t>>& handleToEntity(EntityHandle handle)
	{
		return handleToRawType(handle)->second;
	}

	void addComponentInternal(EntityHandle handle, std::vector<std::pair<uint32_t, uint32_t>> &entity, uint32_t componentID, BaseECSComponent* component);
	BaseECSComponent* getComponentInternal(std::vector<std::pair<uint32_t, uint32_t>>& entityComponents,
		std::vector<uint8_t>& auxArray, uint32_t componentID);
	void deleteComponent(uint32_t componentID, uint32_t index);
	bool removeComponentInternal(EntityHandle handle, uint32_t componentID);

	void updateSystemWithMultipleComponents(uint32_t index, ECSSystemList& systems, float delta, const std::vector<uint32_t>& componentTypes,
		std::vector<BaseECSComponent*>& componentParam, std::vector<std::vector<uint8_t> *> &componentArrays);

	uint8_t findLeastCommonComponent(const std::vector<uint32_t>& componentTypes, const std::vector<uint32_t>& componentFlags);
};