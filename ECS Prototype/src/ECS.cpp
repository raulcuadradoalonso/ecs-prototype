#include "pch.h"
#include "ECS.h"
#include <memory>
#include <algorithm>

ECS::~ECS()
{
	for (std::map<uint32_t, std::vector<uint8_t>>::iterator it = m_components.begin(); it != m_components.end(); ++it)
	{
		size_t typeSize = BaseECSComponent::getTypeSize(it->first);
		ECSComponentFreeFunction freefn = BaseECSComponent::getTypeFreeFunction(it->first);

		for (uint32_t i = 0; i < it->second.size(); i += typeSize)
		{
			freefn((BaseECSComponent*)&it->second[i]);
		}
	}

	for (uint32_t i = 0; i < m_entities.size(); i++)
	{
		delete m_entities[i];
	}
}

EntityHandle ECS::makeEntity(BaseECSComponent** entityComponents, const uint32_t* componentIDs, size_t numComponents)
{
	std::pair<uint32_t, std::vector<std::pair<uint32_t, uint32_t>>>* newEntity = new std::pair<uint32_t, std::vector<std::pair<uint32_t, uint32_t>>>();
	EntityHandle handle = (EntityHandle)newEntity;

	for (uint32_t i = 0; i < numComponents; i++)
	{
		//Check if component ID is actually valid
		if (!BaseECSComponent::isTypeValid(componentIDs[i]))
		{
			//TO DO: Log error
			delete newEntity;
			return NULL_ENTITY_HANDLE;
		}

		addComponentInternal(handle, newEntity->second, componentIDs[i], entityComponents[i]);
	}

	newEntity->first = m_entities.size();
	m_entities.push_back(newEntity);
	return handle;
}

void ECS::removeEntity(EntityHandle handle)
{
	std::vector<std::pair<uint32_t, uint32_t>>& entity = handleToEntity(handle);

	for (uint32_t i = 0; i < entity.size(); i++)
	{
		deleteComponent(entity[i].first, entity[i].second);
	}

	uint32_t destIndex = handleToEntityIndex(handle);
	uint32_t srcIndex = m_entities.size() - 1;

	delete m_entities[destIndex];

	m_entities[destIndex] = m_entities[srcIndex];
	m_entities.pop_back();
}

void ECS::updateSystems(ECSSystemList& systems, float delta)
{
	//Create this vector to not having to reallocate memory for each system
	std::vector<BaseECSComponent*> componentParam;
	std::vector<std::vector<uint8_t> *> componentArrays;

	for (uint32_t i = 0; i < systems.size(); i++)
	{
		const std::vector<uint32_t>& componentTypes = systems[i]->getComponentTypes();

		if (componentTypes.size() == 1)
		{
			size_t typeSize = BaseECSComponent::getTypeSize(componentTypes[0]);
			std::vector<uint8_t>& auxArray = m_components[componentTypes[0]];
			for (uint32_t j = 0; j < auxArray.size(); j += typeSize)
			{
				BaseECSComponent* component = (BaseECSComponent*)&auxArray[j];
				systems[i]->updateComponents(delta, &component);
			}
		}
		else
		{
			updateSystemWithMultipleComponents(i, systems, delta, componentTypes, componentParam, componentArrays);
		}
	}
}

//Currently doesn't support multiple components of the same type on one entity
void ECS::addComponentInternal(EntityHandle handle, std::vector<std::pair<uint32_t, uint32_t>> &entity, uint32_t componentID, BaseECSComponent* component)
{
	ECSComponentCreateFunction createFn = BaseECSComponent::getTypeCreateFunction(componentID);
	std::pair<uint32_t, uint32_t> newPair;
	newPair.first = componentID;
	newPair.second = createFn(m_components[componentID], handle, component);
	entity.push_back(newPair);
}

BaseECSComponent* ECS::getComponentInternal(std::vector<std::pair<uint32_t, uint32_t>>& entityComponents, 
	std::vector<uint8_t>& auxArray, uint32_t componentID)
{
	for (uint32_t i = 0; i < entityComponents.size(); i++)
	{
		if (componentID == entityComponents[i].first)
		{
			return (BaseECSComponent*)&auxArray[entityComponents[i].second];
		}
	}

	return nullptr;
}

bool ECS::removeComponentInternal(EntityHandle handle, uint32_t componentID)
{
	std::vector<std::pair<uint32_t, uint32_t>>& entityComponents = handleToEntity(handle);
	for (uint32_t i = 0; i < entityComponents.size(); i++)
	{
		//This doesn't allow to have multiple components of one type in the same entity
		if (componentID == entityComponents[i].first)
		{
			deleteComponent(entityComponents[i].first, entityComponents[i].second);
			uint32_t srcIndex = entityComponents.size() - 1;
			uint32_t destIndex = i;
			entityComponents[destIndex] = entityComponents[srcIndex];
			entityComponents.pop_back();
			return true;
		}
	}

	return false;
}

void ECS::deleteComponent(uint32_t componentID, uint32_t index)
{
	std::vector<uint8_t>& auxArray = m_components[componentID];
	ECSComponentFreeFunction freeFn = BaseECSComponent::getTypeFreeFunction(componentID);
	size_t typeSize = BaseECSComponent::getTypeSize(componentID);
	uint32_t srcIndex = auxArray.size() - typeSize;

	BaseECSComponent* destComponent = (BaseECSComponent*) &auxArray[index];
	BaseECSComponent* srcComponent = (BaseECSComponent*) &auxArray[srcIndex];

	freeFn(destComponent);

	if (index == srcIndex)
	{
		auxArray.resize(srcIndex);
		return;
	}

	std::memcpy(destComponent, srcComponent, typeSize);

	std::vector<std::pair<uint32_t, uint32_t>>& srcComponents = handleToEntity(srcComponent->entity);
	for (uint32_t i = 0; i < srcComponents.size(); i++)
	{
		//This doesn't allow to have multiple components of one type in the same entity
		if (componentID == srcComponents[i].first && srcIndex == srcComponents[i].second)
		{
			srcComponents[i].second = index;
			break;
		}
	}

	auxArray.resize(srcIndex);
}

void ECS::updateSystemWithMultipleComponents(uint32_t index, ECSSystemList& systems, float delta, const std::vector<uint32_t>& componentTypes, std::vector<BaseECSComponent*>& componentParam,
	std::vector<std::vector<uint8_t> *> &componentArrays)
{
	const std::vector<uint32_t>& componentFlags = systems[index]->getComponentFlags();

	//Initially these vectors have size 0. They'll grow whenever their size is less than the amount of different types of components used in the current system
	componentParam.resize(std::max<size_t>(componentParam.size(), componentTypes.size()));
	componentArrays.resize(std::max<size_t>(componentArrays.size(), componentTypes.size()));

	for (uint32_t i = 0; i < componentTypes.size(); i++)
	{
		componentArrays[i] = &m_components[componentTypes[i]];
	}
	uint32_t minSizeIndex = findLeastCommonComponent(componentTypes, componentFlags);

	size_t typeSize = BaseECSComponent::getTypeSize(componentTypes[minSizeIndex]);
	std::vector<uint8_t>& auxArray = *componentArrays[minSizeIndex];
	for (uint32_t i = 0; i < auxArray.size(); i += typeSize)
	{
		componentParam[minSizeIndex] = (BaseECSComponent *)&auxArray[i];
		std::vector<std::pair<uint32_t, uint32_t>>& entityComponents = handleToEntity(componentParam[minSizeIndex]->entity);

		bool isValid = true;
		for (uint32_t j = 0; j < componentTypes.size(); j++)
		{
			if (j == minSizeIndex)
			{
				continue;
			}

			componentParam[j] = getComponentInternal(entityComponents, *componentArrays[j], componentTypes[j]);
			if (componentParam[j] == nullptr && (componentFlags[j] & BaseECSSystem::FLAG_OPTIONAL) == 0) 
			{			
				isValid = false;
				break;
			}
		}

		if (isValid)
		{
			systems[index]->updateComponents(delta, &componentParam[0]);
		}
	}
}

uint8_t ECS::findLeastCommonComponent(const std::vector<uint32_t>& componentTypes, const std::vector<uint32_t>& componentFlags)
{
	uint32_t minSize = (uint32_t) - 1;
	uint32_t minIndex = (uint32_t) - 1;
	for (uint32_t i = 1; i < componentTypes.size(); i++) 
	{
		if ((componentFlags[i] & BaseECSSystem::FLAG_OPTIONAL) != 0) {
			continue;
		}

		size_t typeSize = BaseECSComponent::getTypeSize(componentTypes[i]);
		uint32_t size = m_components[componentTypes[i]].size() / typeSize;

		if (size <= minSize) {
			minSize = size;
			minIndex = i;
		}
	}

	return minIndex;
}
