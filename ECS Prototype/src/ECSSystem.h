#pragma once
#include "pch.h"
#include "ECSComponent.h"

class BaseECSSystem
{
public:

	enum
	{
		FLAG_OPTIONAL = 1,
	};
	BaseECSSystem() {}

	virtual void updateComponents(float delta, BaseECSComponent** components) {}
	const std::vector<uint32_t>& getComponentTypes()
	{
		return m_componentTypes;
	}

	const std::vector<uint32_t>& getComponentFlags()
	{
		return m_componentFlags;
	}
	bool isValid();

protected:
	void addComponentType(uint32_t componentType, uint32_t componentFlag = 0)
	{
		m_componentTypes.push_back(componentType);
		m_componentFlags.push_back(componentFlag);
	}

private:

	std::vector<uint32_t> m_componentFlags;
	std::vector<uint32_t> m_componentTypes;
};

class ECSSystemList
{
public:

	inline bool addSystem(BaseECSSystem& system)
	{
		if (!system.isValid()) 
		{
			return false;
		}
		m_systems.push_back(&system);
		return true;
	}

	inline size_t size() 
	{
		return m_systems.size();
	}

	inline BaseECSSystem* operator[](uint32_t index) 
	{
		return m_systems[index];
	}

	bool removeSystem(BaseECSSystem& system);

private:

	std::vector<BaseECSSystem*> m_systems;
};