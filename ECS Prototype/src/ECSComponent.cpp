#include "pch.h"
#include "ECSComponent.h"

std::vector <std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t>>* BaseECSComponent::m_componentTypes;

uint32_t BaseECSComponent::registerComponentType(ECSComponentCreateFunction createFn, ECSComponentFreeFunction freeFn, size_t size)
{
	if (m_componentTypes == nullptr) 
	{
		m_componentTypes = new std::vector<std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t> >();
	}
	uint32_t componentID = m_componentTypes->size();
	m_componentTypes->push_back(std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t>(
		createFn, freeFn, size));
	return componentID;
}