#include "pch.h"
#include "ECSSystem.h"

bool BaseECSSystem::isValid()
{
	for (uint32_t i = 0; i < m_componentFlags.size(); i++) {
		if ((m_componentFlags[i] & BaseECSSystem::FLAG_OPTIONAL) == 0) {
			return true;
		}
	}
	return false;
}

bool ECSSystemList::removeSystem(BaseECSSystem& system)
{
	for (uint32_t i = 0; i < m_systems.size(); i++) 
	{
		if (&system == m_systems[i]) 
		{
			m_systems.erase(m_systems.begin() + i);
			return true;
		}
	}
	return false;
}